package be.infernalWhaler.articleOne;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;

public class WebScraperOne {
    public static void main(String[] args) {

        String SEARCH_QUERY = "iphone 6s";
        String BASE_URL = "https://newyork.craigslist.org/";
        WebClient webClient = new WebClient();

        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setJavaScriptEnabled(false);

        try {
            String SEARCH_URL = BASE_URL + "search/sss?sort=rel&query=" + URLEncoder.encode(SEARCH_QUERY,"UTF-8");
            HtmlPage page = webClient.getPage(SEARCH_URL);

            List<?> items = (List<?>) page.getByXPath("//li[@class='result-row']");

            if(items.isEmpty()){
                System.out.println("No items found!");
            } else {
               for(int i = 0; i < items.size(); i++){
                   HtmlElement htmlItem = (HtmlElement) items.get(i);
                   HtmlAnchor htmlAnchor = ((HtmlAnchor) htmlItem.getFirstByXPath(".//p[@class='result-info']/a"));
                   HtmlElement spanPrice = ((HtmlElement) htmlItem.getFirstByXPath(".//a/span[@class='result-price']"));

                   // It is possible that item does not have price, we set price to 0.0
                   String itemPrice = spanPrice == null ? "0.0" : spanPrice.asText();

                   Item item = new Item();
                   item.setTitle(htmlAnchor.asText());
                   item.setUrl(BASE_URL + htmlAnchor.getHrefAttribute());
                   item.setPrice(new BigDecimal(itemPrice.replace("$","")));

                   ObjectMapper mapper = new ObjectMapper();
                   String jsonString = mapper.writeValueAsString(item);

                   System.out.println(jsonString);

               }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
