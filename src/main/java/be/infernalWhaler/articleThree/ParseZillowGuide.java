package be.infernalWhaler.articleThree;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

public class ParseZillowGuide {
    public static void main(String[] args) {
        print("running...");
        Document document;

        try {
            // Get Document obj after parsing the html from given url
            document = Jsoup.connect("http://www.zillow.com/denver-co/").get();
//            document = Jsoup.connect("https://lesmerveillesdumonde.fr/notre-mission/").get();

            String title = document.title();
            print(" Title: " + title);


            Elements price = document.select(".zsg-photo-card-price:contains($)"); //Get price
            Elements address = document.select("span[itemprop]:contains(Denver CO)"); //Get address

            FileOutputStream fileOutputStream=new FileOutputStream("output_zillow.csv");
            PrintStream csv =new PrintStream(fileOutputStream);


            for (int i=0; i < price.size(); i++) {
//                print("ADDRESS: "+address.get(i).text() + " \nPRICE:" + price.get(i).text());
                csv.println("ADDRESS: "+address.get(i).text() + "	        PRICE:" + price.get(i).text());
                csv.println();

            }
            fileOutputStream.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        print("done");
    }


    public static void print(String string) {
        System.out.println(string);
    }
}
