package be.infernalWhaler.articleTwo;



import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class WikiScraper {
    public static void main(String[] args) {
        scrapeTopic("Python");
    }



    public static void scrapeTopic(String url){
        String html = getUrl("http://www.google.com/"+url);
        Document doc = Jsoup.parse(html);
        String contextText =  doc.select("#mw-content-text > p").first().text();
        System.out.println(contextText);
    }

    public static String getUrl(String url){
        URL urlObj = null;
        try {
            urlObj = new URL(url);
        }catch (MalformedURLException mue){
            System.out.println("the URL was malformed!");
            return "";
        }
        URLConnection urlCon = null;
        BufferedReader in = null;
        String outputText = "";
        try {
            urlCon = urlObj.openConnection();
            in = new BufferedReader(new InputStreamReader(urlCon.getInputStream()));
            String line = "";
            while ((line= in.readLine())!= null){
                outputText += line;
            }
            in.close();
        }catch (IOException ioe){
            System.out.println("There was an error connecting to URL");
            return "";
        }
        return outputText;
    }
}
